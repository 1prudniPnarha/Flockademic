import { shallow } from 'enzyme';
import toJson from 'enzyme-to-json';
import * as React from 'react';

import { JournalIndex } from '../../src/components/journalIndex/component';

const mockProps = {
  periodicals: [
    {
      creator: { identifier: 'arbitrary_account_id' },
      datePublished: 'Arbitrary date',
      identifier: 'arbitrary_id',
      name: 'Arbitrary name',
    },
  ],
  url: 'https://flockademic.com/journals',
};

it('should display the journal details', () => {
  const mockPeriodicals = [
    {
      description: 'Some description',
      headline: 'Some headline',
      identifier: 'some_slug',
      name: 'Some name',
    },
  ];
  const overview = shallow(<JournalIndex {...mockProps} periodicals={mockPeriodicals} />);

  expect(toJson(overview)).toMatchSnapshot();
});
